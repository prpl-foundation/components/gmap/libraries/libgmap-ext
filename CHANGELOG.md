# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.2.2 - 2024-12-20(17:22:34 +0000)

### Other

- [libgmap-ext] Remove the search path expression

## Release v1.2.1 - 2024-10-15(11:08:21 +0000)

### Changes

- [SOP] Service identification does not work anymore

## Release v1.2.0 - 2024-09-20(11:53:01 +0000)

### New

- [mapper] MAC address / Hostname mismatch

## Release v1.1.0 - 2024-05-14(07:57:53 +0000)

### Other

- Keep last known IP addresses in the data model

## Release v1.0.0 - 2023-10-25(16:51:55 +0000)

### Other

- [amx][gmap] move non-generic functions out of libgmap-client

## Release v0.8.0 - 2023-08-31(09:03:17 +0000)

### Other

- Remove discoping because moved to libdiscoping

## Release v0.7.0 - 2023-06-15(11:20:28 +0000)

### Other

- update documentation

## Release v0.6.0 - 2023-06-15(10:21:55 +0000)

### Other

- Scan for ipv6 addresses by sending multicast echo request

## Release v0.5.0 - 2023-06-15(08:29:13 +0000)

### Other

- Move fake timers to libamxut (+use libamxut)

## Release v0.4.0 - 2023-06-07(07:50:10 +0000)

### Other

- Support verify reachability of an ipv6 IP

## Release v0.3.3 - 2023-05-26(08:29:47 +0000)

### Other

- Support disabling periodic reverification

## Release v0.3.2 - 2023-05-12(10:27:54 +0000)

### Other

- Drop dependency from ip->watcher on creation of ip

## Release v0.3.1 - 2023-05-09(12:29:47 +0000)

## Release v0.3.0 - 2023-04-20(12:56:23 +0000)

### Other

- Let socketlist deal with arbitrary type of sockets

## Release v0.2.3 - 2023-04-20(08:18:42 +0000)

### Other

- Split gmap_ext_arp_eth_socket from gmap_ext_discoping_socketlist

## Release v0.2.2 - 2023-04-05(08:04:58 +0000)

### Other

- Renames for discoping

## Release v0.2.1 - 2023-02-06(08:47:48 +0000)

### Other

- Track MAC and collisions and reduce ARPs sent

## Release v0.2.0 - 2023-01-17(12:11:55 +0000)

### Other

- send ARP + callback on timeout/success

## Release v0.1.3 - 2023-01-11(10:53:18 +0000)

### Other

- Opensource component

## Release v0.1.2 - 2022-12-16(09:21:25 +0000)

### Other

- Fix file in debian package + description

## Release v0.1.1 - 2022-12-14(08:15:50 +0000)

### Other

- Fix build

## Release v0.1.0 - 2022-12-13(07:25:47 +0000)

### Other

- Receive eth frames + parse ARP + callback gratuitous ARP

