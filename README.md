# libgmap-extensions

`libgmap-extensions` provides functionality that can be used by gmap clients.

This does not include the layer responsible for translating between the gmap-server RPC API and a C API. That is in `libgmap-client`.

This does not include functionality that can be used by both gmap client and gmap-server. That is in `libgmap-common`.

## Discoping

Discoping is moved to [libdiscoping](https://gitlab.com/prpl-foundation/components/core/libraries/libdiscoping/).
