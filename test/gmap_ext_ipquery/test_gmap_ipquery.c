/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_gmap_ipquery.h"
#include "gmap/extensions/ip/gmap_ext_ipquery.h"

#include <stdarg.h> // needed for cmocka
#include <setjmp.h> // needed for cmocka
#include <unistd.h> // needed for cmocka
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include "../common/mock.h"
#include <gmap/gmap.h>
#include <sys/socket.h>
#include <debug/sahtrace.h>
#include <malloc.h>
#include <string.h>
#include <amxd/amxd_transaction.h>

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

int test_gmap_ipquery_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    test_init_dummy_dm(&dm, &parser); // Note: also reads ../common/mock_gmap.odl

    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);
    return 0;
}

int test_gmap_ipquery_teardown(UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);
    bus_ctx = NULL;

    sahTraceClose();
    return 0;
}

typedef struct {
    char* dev_key;
    char* ip;
    bool exists;
    amxc_llist_it_t it;
} seen_item_t;

/** @implements gmap_ipquery_cb_t  */
static void s_ipquery_cb(const char* dev_key,
                         const char* ip,
                         bool ip_exists,
                         void* const userdata) {
    amxc_llist_t* seen_list = userdata;
    seen_item_t* seen_item = calloc(1, sizeof(seen_item_t));
    assert_non_null(seen_list);
    assert_non_null(dev_key);
    assert_non_null(ip);
    amxc_llist_it_init(&seen_item->it);
    seen_item->dev_key = strdup(dev_key);
    seen_item->ip = strdup(ip);
    seen_item->exists = ip_exists;
    amxc_llist_append(seen_list, &seen_item->it);
}

static void s_load_odl(const char* odl_filename) {
    assert_int_equal(0, amxo_parser_parse_file(&parser, odl_filename, amxd_dm_get_root(&dm)));
}

static seen_item_t* s_get_seen_item(amxc_llist_t* seen_list, unsigned int index) {
    amxc_llist_it_t* it = amxc_llist_get_at(seen_list, index);
    assert_non_null(it);
    seen_item_t* item = amxc_llist_it_get_data(it, seen_item_t, it);
    return item;
}


static bool item_present(amxc_llist_t* seen_list, const char* ip, const char* key) {
    seen_item_t* item = NULL;
    amxc_llist_for_each(it, seen_list) {
        item = amxc_container_of(it, seen_item_t, it);
        if((strcmp(ip, item->ip) == 0) && (strcmp(key, item->dev_key) == 0)) {
            return true;
        }
    }
    return false;
}

/** @implements amxc_llist_it_delete_t */
static void s_delete_seen_item(amxc_llist_it_t* it) {
    assert_non_null(it);
    seen_item_t* item = amxc_llist_it_get_data(it, seen_item_t, it);
    assert_non_null(it);
    free(item->dev_key);
    free(item->ip);
    free(item);
}

void test_gmap_ipquery__new_ip(UNUSED void** state) {
    amxc_llist_t seen_list;
    gmap_ipquery_t* ipquery = NULL;
    bool ok = false;
    amxc_llist_init(&seen_list);

    // GIVEN an ipquery
    ok = gmap_ipquery_new(&ipquery, AF_INET, NULL, s_ipquery_cb, &seen_list);
    assert_true(ok);
    assert_non_null(ipquery);

    // WHEN a new IP exists that matches the query criteria
    s_load_odl("mockdata_gmap_devs.odl");
    s_load_odl("mockdata_gmap_dev1ip1_ipv4.odl");
    handle_events();

    // THEN the callback is called for that IP
    assert_int_equal(1, amxc_llist_size(&seen_list));
    assert_string_equal("dev1", s_get_seen_item(&seen_list, 0)->dev_key);
    assert_string_equal("192.168.1.11", s_get_seen_item(&seen_list, 0)->ip);
    assert_true(s_get_seen_item(&seen_list, 0)->exists);

    amxc_llist_clean(&seen_list, s_delete_seen_item);
    gmap_ipquery_delete(&ipquery);
}

void test_gmap_ipquery__delete_ip(UNUSED void** state) {
    amxc_llist_t seen_list;
    gmap_ipquery_t* ipquery = NULL;
    amxd_trans_t trans;
    amxc_llist_init(&seen_list);

    // GIVEN IPs and an ipquery
    s_load_odl("mockdata_gmap_devs.odl");
    s_load_odl("mockdata_gmap_dev1ip1_ipv4.odl");
    handle_events();
    gmap_ipquery_new(&ipquery, AF_INET, NULL, s_ipquery_cb, &seen_list);
    assert_int_equal(1, amxc_llist_size(&seen_list));

    // WHEN an IP is deleted
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Devices.Device.dev1.IPv4Address.");
    amxd_trans_del_inst(&trans, 1, NULL);
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, &dm));
    amxd_trans_clean(&trans);
    handle_events();

    // THEN the callback is called for that IP
    assert_int_equal(2, amxc_llist_size(&seen_list));
    assert_string_equal("dev1", s_get_seen_item(&seen_list, 1)->dev_key);
    assert_string_equal("192.168.1.11", s_get_seen_item(&seen_list, 1)->ip);
    assert_false(s_get_seen_item(&seen_list, 1)->exists);

    amxc_llist_clean(&seen_list, s_delete_seen_item);
    gmap_ipquery_delete(&ipquery);
}

void test_gmap_ipquery__change_ip(UNUSED void** state) {
    amxc_llist_t seen_list;
    gmap_ipquery_t* ipquery = NULL;
    amxd_trans_t trans;
    amxc_llist_init(&seen_list);

    // GIVEN IPs and an ipquery
    s_load_odl("mockdata_gmap_devs.odl");
    s_load_odl("mockdata_gmap_dev1ip1_ipv4.odl");
    handle_events();
    gmap_ipquery_new(&ipquery, AF_INET, NULL, s_ipquery_cb, &seen_list);
    assert_int_equal(1, amxc_llist_size(&seen_list));

    // WHEN an IP changes to another IP
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Devices.Device.dev1.IPv4Address.1.");
    amxd_trans_set_cstring_t(&trans, "Address", "192.168.5.5");
    assert_int_equal(amxd_status_ok, amxd_trans_apply(&trans, &dm));
    amxd_trans_clean(&trans);
    handle_events();

    // THEN the callback was called for the initial list (not relevant here),
    //      the removal of the old IP
    //      and addition of the new IP (in that order)
    assert_int_equal(3, amxc_llist_size(&seen_list));
    // old ip:
    assert_string_equal("dev1", s_get_seen_item(&seen_list, 1)->dev_key);
    assert_string_equal("192.168.1.11", s_get_seen_item(&seen_list, 1)->ip);
    assert_false(s_get_seen_item(&seen_list, 1)->exists); // <-- deleted
    // new ip:
    assert_string_equal("dev1", s_get_seen_item(&seen_list, 2)->dev_key);
    assert_string_equal("192.168.5.5", s_get_seen_item(&seen_list, 2)->ip);
    assert_true(s_get_seen_item(&seen_list, 2)->exists); // <-- added

    amxc_llist_clean(&seen_list, s_delete_seen_item);
    gmap_ipquery_delete(&ipquery);
}

void test_gmap_ipquery__filtering(UNUSED void** state) {
    amxc_llist_t seen_list;
    gmap_ipquery_t* ipquery = NULL;
    bool ok = false;
    amxc_llist_init(&seen_list);

    // GIVEN an ipquery
    ok = gmap_ipquery_new(&ipquery, AF_INET6, NULL, s_ipquery_cb, &seen_list);
    assert_true(ok);
    assert_non_null(ipquery);

    // WHEN a lot of IP addresses are added in gmap but only one matches the "tag2" tag and ipv6 family:
    s_load_odl("mockdata_gmap_devs.odl");
    s_load_odl("mockdata_gmap_all_ips.odl");
    handle_events();

    // THEN the callback is called for the matching IP,
    //      so not for 192.168.1.22 (ip family mismatch)
    assert_int_equal(2, amxc_llist_size(&seen_list));
    // first ip:
    assert_string_equal("dev1", s_get_seen_item(&seen_list, 0)->dev_key);
    assert_string_equal("fe80::a2b5:49ff:fe0a:1001", s_get_seen_item(&seen_list, 0)->ip);
    assert_true(s_get_seen_item(&seen_list, 0)->exists);

    // second ip:
    assert_string_equal("dev2", s_get_seen_item(&seen_list, 1)->dev_key);
    assert_string_equal("fe80::a2b5:49ff:fe0a:2001", s_get_seen_item(&seen_list, 1)->ip);
    assert_true(s_get_seen_item(&seen_list, 1)->exists);



    amxc_llist_clean(&seen_list, s_delete_seen_item);
    gmap_ipquery_delete(&ipquery);

    handle_events();
}

void test_gmap_ipquery__initialpopulation(UNUSED void** state) {
    amxc_llist_t seen_list;
    gmap_ipquery_t* ipquery = NULL;
    bool ok = false;
    amxc_llist_init(&seen_list);

    // GIVEN a bunch of IP addresses
    s_load_odl("mockdata_gmap_devs.odl");
    s_load_odl("mockdata_gmap_all_ips.odl");
    handle_events();

    // WHEN creating an ipquery
    ok = gmap_ipquery_new(&ipquery, AF_INET, NULL, s_ipquery_cb, &seen_list);
    assert_true(ok);
    assert_non_null(ipquery);
    handle_events();

    // THEN the callback is called for the initially already-existing IP adresses
    assert_int_equal(2, amxc_llist_size(&seen_list));
    assert_true(item_present(&seen_list, "192.168.1.22", "dev2"));
    assert_true(s_get_seen_item(&seen_list, 0)->exists);
    assert_true(item_present(&seen_list, "192.168.1.11", "dev1"));
    assert_true(s_get_seen_item(&seen_list, 1)->exists);
    amxc_llist_clean(&seen_list, s_delete_seen_item);
    gmap_ipquery_delete(&ipquery);

    handle_events();
}
