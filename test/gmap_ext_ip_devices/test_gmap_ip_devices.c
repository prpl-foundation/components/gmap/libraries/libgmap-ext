/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <unistd.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include <sys/socket.h>

#include "../common/mock.h"
#include "test_gmap_ip_devices.h"
#include <gmap/gmap.h>

#include "gmap/extensions/ip/gmap_ext_ip_devices.h"


static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx;


void test_gmap_findByIp(UNUSED void** state) {
    char* dev_key = NULL;
    expect_string_count(__wrap_gmap_devices_find, expression, "has_instance(\"IPv4Address\", \".Address ^= '192.168.1.10' && .Status == 'reachable'\")", 1);
    expect_value_count(__wrap_gmap_devices_find, flags, 0, 1);

    will_return(__wrap_gmap_devices_find, true);
    dev_key = gmap_devices_findByIp("192.168.1.10");
    assert_string_equal(dev_key, "mock_dev");

    free(dev_key);
}

int test_gmap_findByIp_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    test_init_dummy_dm(&dm, &parser);
    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);

    handle_events();

    return 0;
}

int test_gmap_findByIp_teardown(UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);
    bus_ctx = NULL;

    sahTraceClose();

    return 0;
}

bool __wrap_gmap_devices_find(const char* expression,
                              uint32_t flags,
                              amxc_var_t* ret) {
    amxc_var_t* mock_data = NULL;

    check_expected_ptr(expression);
    check_expected(flags);

    mock_data = amxc_var_add_new(ret);
    amxc_var_set_type(mock_data, AMXC_VAR_ID_LIST);

    amxc_var_add(cstring_t, mock_data, "mock_dev");


    return (bool) mock();
}
