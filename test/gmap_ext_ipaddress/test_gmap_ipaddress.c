/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include "gmap/extensions/ip/gmap_ext_ipaddress.h"
#include "test_gmap_ipaddress.h"
#include "../common/mock.h"
#include <gmap/gmap.h>
#include <gmap/gmap.h>
#include <sys/socket.h>
#include <debug/sahtrace.h>

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;


int test_gmap_ipaddress_setup(UNUSED void** state) {
    sahTraceOpen("test", TRACE_TYPE_STDERR);
    sahTraceSetLevel(TRACE_LEVEL_ERROR);
    sahTraceSetTimeFormat(TRACE_TIME_APP_SECONDS);

    test_init_dummy_dm(&dm, &parser); // Note: also reads ../common/mock_gmap.odl

    bus_ctx = amxb_be_who_has("Devices");
    gmap_client_init(bus_ctx);

    return 0;
}

int test_gmap_ipaddress_teardown(UNUSED void** state) {
    test_clean_dummy_dm(&dm, &parser);
    bus_ctx = NULL;

    sahTraceClose();
    return 0;
}

void test_gmap_add(UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    expect_string_count(__wrap_addAddressInstance, address, "10.10.10.10", 1);
    expect_string_count(__wrap_addAddressInstance, scope, "global", 1);
    expect_string_count(__wrap_addAddressInstance, status, "reachable", 1);
    expect_string_count(__wrap_addAddressInstance, source, "testSource", 1);
    expect_value_count(__wrap_addAddressInstance, reserved, 1, 1);
    will_return(__wrap_addAddressInstance, 0);

    status = gmap_ip_device_add_address("dev1", gmap_ip_family_string2id("ipv4"), "10.10.10.10", "global", "reachable", "testSource", true);
    assert_int_equal((int) status, (int) amxd_status_ok);
}

static void s_assert_dev1ip1(const amxc_var_t* ip1) {
    assert_non_null(ip1);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, ip1)), 5);
    assert_string_equal("192.168.1.1", GET_CHAR(ip1, "Address"));
    assert_string_equal("not reachable", GET_CHAR(ip1, "Status"));
    assert_string_equal("unknown", GET_CHAR(ip1, "Scope"));
    assert_string_equal("my addr source", GET_CHAR(ip1, "AddressSource"));
    assert_true(GET_BOOL(ip1, "Reserved"));
}

void test_gmap_ip_device_get_adress__normalcase(UNUSED void** state) {
    amxc_var_t ip;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting one IP address
    status = gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv4"), "192.168.1.1", &ip);

    // THEN the data contains the fields that we loaded above (of the correct IP)
    assert_int_equal(status, amxd_status_ok);
    s_assert_dev1ip1(&ip);

    amxc_var_clean(&ip);
}

void test_gmap_ip_device_get_adress__ip_not_found(UNUSED void** state) {
    amxc_var_t ip;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting an IP address that does not exist
    status = gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv4"), "192.168.1.4", &ip);

    // THEN an error is returned
    assert_int_not_equal(status, amxd_status_ok);

    amxc_var_clean(&ip);
}

void test_gmap_ip_device_get_adress__wrong_device(UNUSED void** state) {
    amxc_var_t ip;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting an IP address on the wrong device
    status = gmap_ip_device_get_address("dev0", gmap_ip_family_string2id("ipv4"), "192.168.1.2", &ip);

    // THEN an error is returned
    assert_int_not_equal(status, amxd_status_ok);

    amxc_var_clean(&ip);
}

void test_gmap_ip_device_get_adress__device_does_not_exist(UNUSED void** state) {
    amxc_var_t ip;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting an IP address on the wrong device
    status = gmap_ip_device_get_address("nonexisting_device", gmap_ip_family_string2id("ipv4"), "192.168.1.2", &ip);

    // THEN an error is returned
    assert_int_not_equal(status, amxd_status_ok);

    amxc_var_clean(&ip);
}

void test_gmap_ip_device_get_adress__ipv6(UNUSED void** state) {
    amxc_var_t ip;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ip);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting an IP address on the wrong device
    status = gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv6"), "fe80::a2b5:49ff:fe0a:3000", &ip);

    // THEN the data contains the fields that we loaded above (of the correct IP)
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &ip)), 4);
    assert_string_equal("fe80::a2b5:49ff:fe0a:3000", GET_CHAR(&ip, "Address"));
    assert_string_equal("reachable", GET_CHAR(&ip, "Status"));
    assert_string_equal("link", GET_CHAR(&ip, "Scope"));
    assert_string_equal("source3", GET_CHAR(&ip, "AddressSource"));

    amxc_var_clean(&ip);
}

void test_gmap_ip_device_get_adress__invalid_argument(UNUSED void** state) {
    amxc_var_t ips;
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));
    amxc_var_init(&ips);

    // Invalid key:
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address(NULL, gmap_ip_family_string2id("ipv4"), "192.168.1.1", &ips));
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address("", gmap_ip_family_string2id("ipv4"), "192.168.1.1", &ips));

    // Invalid ip family:
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address("dev1", 0, "192.168.1.1", &ips));

    // Invalid IP
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv4"), "", &ips));
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv4"), NULL, &ips));

    // Invalid target variant:
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_address("dev1", gmap_ip_family_string2id("ipv4"), "192.168.1.1", NULL));

    amxc_var_clean(&ips);
}

void test_gmap_ip_device_get_adresses__normalcase(UNUSED void** state) {
    amxc_var_t ips;
    const amxc_var_t* ip1;
    const amxc_var_t* ip2;
    const amxc_var_t* ip3;
    const amxc_var_t* ip4;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ips);

    // GIVEN a gmap datamodel that contains a number of IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting the addresses
    status = gmap_ip_device_get_addresses("dev1", &ips);

    // THEN the data contains all IP addresses
    assert_int_equal(status, amxd_status_ok);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &ips)), 4);
    // ip1:
    ip1 = GET_ARG(&ips, "192.168.1.1");
    s_assert_dev1ip1(ip1);
    // ip2:
    ip2 = GET_ARG(&ips, "192.168.1.2");
    assert_non_null(ip2);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, ip2)), 5);
    assert_string_equal("192.168.1.2", GET_CHAR(ip2, "Address"));
    assert_string_equal("reachable", GET_CHAR(ip2, "Status"));
    assert_string_equal("global", GET_CHAR(ip2, "Scope"));
    assert_string_equal("addr source 2", GET_CHAR(ip2, "AddressSource"));
    assert_false(GET_BOOL(ip2, "Reserved"));
    // ip3:
    ip3 = GET_ARG(&ips, "fe80::a2b5:49ff:fe0a:3000");
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, ip3)), 4);
    assert_string_equal("fe80::a2b5:49ff:fe0a:3000", GET_CHAR(ip3, "Address"));
    assert_string_equal("reachable", GET_CHAR(ip3, "Status"));
    assert_string_equal("link", GET_CHAR(ip3, "Scope"));
    assert_string_equal("source3", GET_CHAR(ip3, "AddressSource"));
    // ip4:
    ip4 = GET_ARG(&ips, "fe80::a2b5:49ff:fe0a:4000");
    assert_non_null(ip4);
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, ip4)), 4);
    assert_string_equal("fe80::a2b5:49ff:fe0a:4000", GET_CHAR(ip4, "Address"));
    assert_string_equal("not reachable", GET_CHAR(ip4, "Status"));
    assert_string_equal("link", GET_CHAR(ip4, "Scope"));
    assert_string_equal("source4", GET_CHAR(ip4, "AddressSource"));

    amxc_var_clean(&ips);
}

void test_gmap_ip_device_get_adresses__no_address(UNUSED void** state) {
    amxc_var_t ips;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_init(&ips);

    // GIVEN a gmap datamodel with a device that has no IP addresses
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    // WHEN getting the addresses of that ip-less device
    status = gmap_ip_device_get_addresses("dev_noip", &ips);

    // THEN no error is returned
    assert_int_equal(status, amxd_status_ok);
    // THEN no addresses are returned
    assert_int_equal(AMXC_VAR_ID_HTABLE, amxc_var_type_of(&ips));
    assert_int_equal(amxc_htable_size(amxc_var_constcast(amxc_htable_t, &ips)), 0);

    amxc_var_clean(&ips);
}

void test_gmap_ip_device_get_adresses__invalid_argument(UNUSED void** state) {
    amxc_var_t ips;
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));
    amxc_var_init(&ips);

    // Invalid device key:
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_addresses(NULL, &ips));
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_addresses("", &ips));

    // Invalid target variant:
    assert_int_not_equal(amxd_status_ok, gmap_ip_device_get_addresses("dev1", NULL));

    amxc_var_clean(&ips);
}

void test_gmap_set(UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    expect_string_count(__wrap_setAddressInstance, address, "192.168.1.1", 1);
    expect_string_count(__wrap_setAddressInstance, scope, "global", 1);
    expect_string_count(__wrap_setAddressInstance, status, "reachable", 1);
    expect_string_count(__wrap_setAddressInstance, source, "testSource", 1);
    expect_value_count(__wrap_setAddressInstance, reserved, 1, 1);
    will_return(__wrap_setAddressInstance, 0);

    status = gmap_ip_device_set_address("dev1", gmap_ip_family_string2id("ipv4"), "192.168.1.1", "global", "reachable", "testSource", true);
    assert_int_equal((int) status, (int) amxd_status_ok);
}

void test_gmap_del(UNUSED void** state) {
    amxd_status_t status = amxd_status_unknown_error;
    assert_int_equal(0, amxo_parser_parse_file(&parser, "mock_gmap_address_data.odl", amxd_dm_get_root(&dm)));

    expect_string_count(__wrap_deleteAddressInstance, address, "10.10.10.10", 1);
    will_return(__wrap_deleteAddressInstance, 0);

    status = gmap_ip_device_delete_address("dev1", gmap_ip_family_string2id("ipv4"), "10.10.10.10");
    assert_int_equal((int) status, (int) amxd_status_ok);
}

void test_ip_family(UNUSED void** state) {
    assert_int_equal(gmap_ip_family("192.159.30.38"), AF_INET);
    assert_int_equal(gmap_ip_family("2001:db8:0:1:1:1:1:1"), AF_INET6);
}
void test_ip_family_string2id(UNUSED void** state) {
    assert_int_equal(gmap_ip_family_string2id("ipv4"), AF_INET);
    assert_int_equal(gmap_ip_family_string2id("ipv6"), AF_INET6);
    assert_int_equal(gmap_ip_family_string2id("iPV4"), AF_INET);
    assert_int_equal(gmap_ip_family_string2id("IPv6"), AF_INET6);
}

void test_ip_address_scope2string(UNUSED void** state) {
    assert_string_equal(gmap_ip_address_scope2string(scope_link), "link");
    assert_string_equal(gmap_ip_address_scope2string(scope_site), "site");
    assert_string_equal(gmap_ip_address_scope2string(scope_global), "global");
    assert_string_equal(gmap_ip_address_scope2string(scope_unknown), "unknown");
    assert_string_equal(gmap_ip_address_scope2string((gmap_ip_address_scope_t) 4), "unknown");
}

void test_ip_addressScope(UNUSED void** state) {
    gmap_ip_address_scope_t ret_scope;
    amxd_status_t status = amxd_status_ok;
    status = gmap_ip_addressScope(AF_INET, "w", &ret_scope);
    assert_int_not_equal((int) status, (int) amxd_status_ok);

    status = gmap_ip_addressScope(AF_INET, "2001:db8:0:1:1:1:1:1", &ret_scope);
    assert_int_not_equal((int) status, (int) amxd_status_ok);

    status = gmap_ip_addressScope(AF_INET6, "192.169.0.195", &ret_scope);
    assert_int_not_equal((int) status, (int) amxd_status_ok);

    status = gmap_ip_addressScope(-1, "192.169.0.195", &ret_scope);
    assert_int_not_equal((int) status, (int) amxd_status_ok);

    //test different types of ipv4 scopes
    status = gmap_ip_addressScope(AF_INET, "192.169.0.195", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_global);

    status = gmap_ip_addressScope(AF_INET, "169.253.1.1", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_global);

    status = gmap_ip_addressScope(AF_INET, "169.254.1.1", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_link);

    //test different types of ipv6 scopes
    status = gmap_ip_addressScope(AF_INET6, "FE80::", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_link);

    status = gmap_ip_addressScope(AF_INET6, "FEC0::", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_site);

    status = gmap_ip_addressScope(AF_INET6, "EE80::", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_global);

    status = gmap_ip_addressScope(AF_INET6, "FF80::", &ret_scope);
    assert_int_equal((int) status, (int) amxd_status_ok);
    assert_int_equal((int) ret_scope, (int) scope_global);
}


int __wrap_amxb_add(amxb_bus_ctx_t* const bus,
                    const char* object,
                    uint32_t index,
                    const char* name,
                    amxc_var_t* values,
                    amxc_var_t* ret,
                    int timeout) {
    check_expected_ptr(bus);
    check_expected_ptr(object);
    check_expected_ptr(index);
    check_expected(timeout);
    check_expected(name);
    assert_int_equal(amxc_var_type_of(values), AMXC_VAR_ID_HTABLE);

    amxc_var_set(bool, ret, true);
    return (int) mock();
}

int32_t __wrap_gmap_add_instance(const char* key,
                                 const char* path,
                                 amxc_var_t* values) {
    check_expected(key);
    check_expected(path);
    assert_int_equal(amxc_var_type_of(values), AMXC_VAR_ID_HTABLE);

    return (int) mock();
}


int __wrap_amxb_set(amxb_bus_ctx_t* const bus,
                    const char* object,
                    amxc_var_t* values,
                    amxc_var_t* ret,
                    int timeout) {
    check_expected_ptr(bus);
    check_expected_ptr(object);
    check_expected(timeout);

    assert_int_equal(amxc_var_type_of(values), AMXC_VAR_ID_HTABLE);

    amxc_var_set(bool, ret, true);
    return (int) mock();
}

uint32_t __wrap_gmap_set_subobject_instance(const char* key,
                                            const char* path,
                                            amxc_var_t* values) {
    check_expected(key);
    check_expected(path);

    assert_int_equal(amxc_var_type_of(values), AMXC_VAR_ID_HTABLE);

    return (int) mock();
}

int __wrap_amxb_del(amxb_bus_ctx_t* const bus,
                    const char* object,
                    uint32_t index,
                    const char* name,
                    amxc_var_t* ret,
                    int timeout) {
    check_expected_ptr(bus);
    check_expected_ptr(object);
    check_expected_ptr(index);
    check_expected_ptr(name);
    check_expected(timeout);

    amxc_var_set(bool, ret, true);
    return (int) mock();
}

uint32_t __wrap_gmap_delete_instance(const char* key,
                                     const char* path,
                                     uint32_t index) {
    check_expected(key);
    check_expected(path);
    check_expected(index);

    return (int) mock();
}
