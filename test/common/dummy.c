/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include <amxb/amxb.h>

#include "mock.h"

typedef struct {
    amxc_htable_it_t it;
    char* name;
    char* expression;
    uint32_t id;
    uint32_t index;
} dummy_query_t;

typedef struct {
    amxc_htable_it_t it;
    char* key;
    char* tags;
} dummy_device_t;

static const char* const odl_mock_gmap = "../common/mock_gmap.odl";
static amxd_dm_t* dummy_dm = NULL;
static amxc_htable_t* queries = NULL;
static amxc_htable_t* devices = NULL;


void handle_events(void) {
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

static amxd_status_t _dummy_bool(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

static amxd_status_t _dummy_void(UNUSED amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 UNUSED amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}


static void add_matching_devices(const char* expression, amxc_var_t* matching_devices) {
    printf("add matching devices\n");
    amxc_var_set_type(matching_devices, AMXC_VAR_ID_LIST);
    amxc_htable_for_each(it, devices) {
        const char* devicekey = amxc_htable_it_get_key(it);
        printf("device key: %s\n", devicekey);
        dummy_device_t* dummy_device = amxc_htable_it_get_data(it, dummy_device_t, it);
        printf("device tags: %s\n", dummy_device->tags);
        printf("expression: %s\n", expression);
        if(!strcmp(expression, dummy_device->tags)) {
            amxc_var_t* device = amxc_var_add_new(matching_devices);
            amxc_var_set_type(device, AMXC_VAR_ID_HTABLE);
            amxc_var_add_key(cstring_t, device, "Key", devicekey);
        }
    }
}

static amxd_status_t _createDevice(amxd_object_t* object,
                                   amxd_function_t* func,
                                   amxc_var_t* args,
                                   amxc_var_t* ret) {

    amxd_status_t amxd_status = amxd_status_ok;

    if(devices == NULL) {
        amxc_htable_new(&devices, 0);
    }

    dummy_device_t* device = calloc(1, sizeof(dummy_device_t));
    device->tags = strdup(GET_CHAR(args, "tags"));
    device->key = strdup(GET_CHAR(args, "key"));
    printf("insert: key: %s\n", device->key);
    amxc_htable_insert(devices, device->key, &device->it);
    amxd_status = _dummy_bool(object, func, args, ret);
    return amxd_status;
}

static amxd_status_t _destroyDevice(amxd_object_t* object,
                                    amxd_function_t* func,
                                    amxc_var_t* args,
                                    amxc_var_t* ret) {

    amxd_status_t amxd_status = amxd_status_ok;
    amxc_htable_it_t* it = NULL;

    it = amxc_htable_take(devices, GET_CHAR(args, "key"));
    if(it) {
        printf("free device\n");
        dummy_device_t* device = amxc_htable_it_get_data(it, dummy_device_t, it);
        amxc_htable_it_clean(&device->it, NULL);
        it = NULL;
        free(device->tags);
        free(device->key);
        free(device);
    }
    if(amxc_htable_is_empty(devices)) {
        printf("empty\n");
        amxc_htable_delete(&devices, NULL);
    }

    amxd_status = _dummy_bool(object, func, args, ret);
    return amxd_status;
}

static amxd_status_t _matchingDevices(amxd_object_t* object,
                                      UNUSED amxd_function_t* func,
                                      UNUSED amxc_var_t* args,
                                      amxc_var_t* ret) {
    printf("_matchingDevices\n");
    amxd_status_t amxd_status = amxd_status_ok;
    char* expression = amxd_object_get_value(cstring_t, object, "Expression", NULL);
    printf("matching expression: %s\n", expression);
    assert_non_null(expression);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    amxc_htable_for_each(it, devices) {
        const char* devicekey = amxc_htable_it_get_key(it);
        printf("device key: %s\n", devicekey);
        dummy_device_t* dummy_device = amxc_htable_it_get_data(it, dummy_device_t, it);
        printf("device tags: %s\n", dummy_device->tags);
        printf("expression: %s\n", expression);
        if(!strcmp(expression, dummy_device->tags)) {
            amxc_var_add(cstring_t,
                         ret,
                         devicekey
                         );
        }
    }
    free(expression);
    return amxd_status;
}

static amxd_status_t _openQuery(UNUSED amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {

    amxd_status_t amxd_status = amxd_status_ok;
    amxd_object_t* query_obj = NULL;
    /* amxd_object_t* query_tmpl_obj = NULL; */
    amxd_trans_t trans;
    dummy_query_t* query = NULL;
    amxc_var_t* matching_devices = NULL;
    printf("bob\n");

    if(queries == NULL) {
        amxc_htable_new(&queries, 0);
    }

    query = calloc(1, sizeof(dummy_query_t));
    query->expression = strdup(GET_CHAR(args, "expression"));
    query->name = strdup(GET_CHAR(args, "name"));
    assert_non_null(query->expression);
    assert_non_null(query->name);
    printf("query name:%s\n", query->name);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Devices.Query.");
    amxd_trans_add_inst(&trans, 0, query->name);
    amxd_trans_set_value(cstring_t, &trans, "Expression", query->expression);
    amxd_trans_set_value(cstring_t, &trans, "Name", query->name);
    assert_int_equal(amxd_trans_apply(&trans, dummy_dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    query_obj = amxd_dm_findf(dummy_dm, "Devices.Query.%s.", query->name);
    assert_non_null(query_obj);
    query->index = amxd_object_get_index(query_obj);
    printf("query index: %d\n", query->index);
    query->id = 1;
    char index_str[5];
    sprintf(index_str, "%d", query->index);
    amxc_htable_insert(queries, index_str, &query->it);

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, ret, "id", query->id);
    amxc_var_add_key(uint32_t, ret, "index", query->index);
    amxc_var_add_key(cstring_t, ret, "name", query->name);

    matching_devices = amxc_var_add_new_key(ret, "devices");
    add_matching_devices(query->expression, matching_devices);

    return amxd_status;

}

static amxd_status_t _closeQuery(amxd_object_t* object,
                                 amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxc_htable_it_t* it = NULL;
    amxd_status_t amxd_status = amxd_status_ok;

    uint32_t index = GET_INT32(args, "index");
    char index_str[5];
    sprintf(index_str, "%d", index);

    printf("close query\n");
    it = amxc_htable_take(queries, index_str);
    if(it != NULL) {
        dummy_query_t* q = amxc_htable_it_get_data(it, dummy_query_t, it);
        amxc_htable_it_clean(&q->it, NULL);
        it = NULL;
        free(q->expression);
        free(q->name);
        free(q);
        if(amxc_htable_is_empty(queries)) {
            amxc_htable_delete(&queries, NULL);
        }
    }
    amxd_status = _dummy_void(object, func, args, ret);
    return amxd_status;
}

static amxd_status_t _notify(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    const char* key = GET_CHAR(args, "key");
    uint32_t event_id = GET_UINT32(args, "id");
    // const char* event_name = GET_CHAR(args, "name");
    amxc_var_t* data = GET_ARG(args, "data");
    amxc_var_t sig_data;
    amxc_var_t* event_data = NULL;
    amxd_object_t* device_obj = NULL;
    assert_non_null(key);
    assert_string_not_equal(key, "");
    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    event_data = amxc_var_add_new_key(&sig_data, "Data");
    amxc_var_copy(event_data, data);
    amxc_var_add_key(uint32_t,
                     (amxc_var_t*) &sig_data,
                     "EventId", event_id);
    amxc_var_add_key(cstring_t,
                     (amxc_var_t*) &sig_data,
                     "Key", key);
    printf("emit gmap_event key:%s \n", key);
    device_obj = amxd_dm_findf(dummy_dm, "Devices.Device.%s.", "dummy");
    assert_non_null(device_obj);
    amxd_object_emit_signal(device_obj, "gmap_event", &sig_data);
    amxc_var_clean(&sig_data);
    return amxd_status_ok;
}

static amxd_status_t _setFunction(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  UNUSED amxc_var_t* args,
                                  amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

static amxd_status_t _removeFunction(UNUSED amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

static amxd_status_t _isImplemented(UNUSED amxd_object_t* object,
                                    UNUSED amxd_function_t* func,
                                    UNUSED amxc_var_t* args,
                                    amxc_var_t* ret) {
    amxc_var_set(bool, ret, true);
    return amxd_status_ok;
}

static amxd_status_t _csiFinished(UNUSED amxd_object_t* object,
                                  UNUSED amxd_function_t* func,
                                  UNUSED amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

static uint32_t __wrap_deleteAddressInstance(UNUSED amxd_object_t* object,
                                             UNUSED amxd_function_t* func,
                                             UNUSED amxc_var_t* args,
                                             UNUSED amxc_var_t* ret) {
    const char* address = GET_CHAR(args, "address");
    check_expected(address);

    return (int) mock();
}

static uint32_t __wrap_addAddressInstance(UNUSED amxd_object_t* object,
                                          UNUSED amxd_function_t* func,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    const char* address = GET_CHAR(args, "address");
    const char* source = GET_CHAR(args, "source");
    const char* status = GET_CHAR(args, "status");
    const char* scope = GET_CHAR(args, "scope");
    bool reserved = GET_BOOL(args, "reserved");

    check_expected(address);
    check_expected(status);
    check_expected(scope);
    check_expected(source);
    check_expected(reserved);

    return (int) mock();
}

static uint32_t __wrap_setAddressInstance(UNUSED amxd_object_t* object,
                                          UNUSED amxd_function_t* func,
                                          amxc_var_t* args,
                                          UNUSED amxc_var_t* ret) {
    const char* address = GET_CHAR(args, "address");
    const char* source = GET_CHAR(args, "source");
    const char* status = GET_CHAR(args, "status");
    const char* scope = GET_CHAR(args, "scope");
    bool reserved = GET_BOOL(args, "reserved");

    check_expected(address);
    check_expected(status);
    check_expected(scope);
    check_expected(source);
    check_expected(reserved);

    return (int) mock();
}


static void test_init_dummy_fn_resolvers(amxo_parser_t* parser) {
    amxo_resolver_ftab_add(parser, "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(parser, "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(parser, "createDevice", AMXO_FUNC(_createDevice));
    amxo_resolver_ftab_add(parser, "destroyDevice", AMXO_FUNC(_destroyDevice));
    amxo_resolver_ftab_add(parser, "matchingDevices", AMXO_FUNC(_matchingDevices));
    amxo_resolver_ftab_add(parser, "notify", AMXO_FUNC(_notify));
    amxo_resolver_ftab_add(parser, "setFunction", AMXO_FUNC(_setFunction));
    amxo_resolver_ftab_add(parser, "removeFunction", AMXO_FUNC(_removeFunction));
    amxo_resolver_ftab_add(parser, "isImplemented", AMXO_FUNC(_isImplemented));
    amxo_resolver_ftab_add(parser, "csiFinished", AMXO_FUNC(_csiFinished));
    amxo_resolver_ftab_add(parser, "deleteAddressInstance", AMXO_FUNC(__wrap_deleteAddressInstance));
    amxo_resolver_ftab_add(parser, "addAddressInstance", AMXO_FUNC(__wrap_addAddressInstance));
    amxo_resolver_ftab_add(parser, "setAddressInstance", AMXO_FUNC(__wrap_setAddressInstance));
}

void test_init_dummy_dm(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(dm), amxd_status_ok);
    dummy_dm = dm;
    assert_int_equal(amxo_parser_init(parser), 0);

    test_register_dummy_be();
    test_init_dummy_fn_resolvers(parser);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(parser, odl_mock_gmap, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx), 0);
    // Register data model
    assert_int_equal(amxb_register(bus_ctx, dm), 0);
}

void test_clean_dummy_dm(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxo_parser_clean(parser);
    amxd_dm_clean(dm);
    test_unregister_dummy_be();
    dummy_dm = NULL;
}


