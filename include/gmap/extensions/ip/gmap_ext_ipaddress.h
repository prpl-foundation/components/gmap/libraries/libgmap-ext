/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_IPADDRESS_H__)
#define __GMAP_IPADDRESS_H__

#ifdef __cplusplus
extern "C"
{
#endif

#if !defined(USE_DOXYGEN)
#define GMAP_INLINE static inline
#else
#define GMAP_INLINE
#endif
#include <stdbool.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>



/**********************************************************
* Macro definitions
**********************************************************/

#define GMAP_IP_SCOPE_GLOBAL "global"
#define GMAP_IP_SCOPE_SITE "site"
#define GMAP_IP_SCOPE_LINK "link"
#define GMAP_IP_SCOPE_UNKNOWN "unknown"

#define GMAP_IP_STATUS_REACHABLE "reachable"
#define GMAP_IP_STATUS_NOT_REACHABLE "not reachable"
#define GMAP_IP_STATUS_ERROR "error"

/**********************************************************
* Type definitions
**********************************************************/

typedef enum _gmap_ip_address_scope {
    scope_unknown = 0,
    scope_link = 1,
    scope_site = 2,
    scope_global = 3,
} gmap_ip_address_scope_t;


/**********************************************************
* Function Prototypes
**********************************************************/

/**
 *  add an address to a specific device
 */
amxd_status_t gmap_ip_device_add_address(const char* key, uint32_t family, const char* address, const char* scope, const char* status_value, const char* address_source, bool reserved);

/**
 *  delete an address from a specific device
 */
amxd_status_t gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address);

/**
 *  get an address from a specific device
 */
amxd_status_t gmap_ip_device_get_address(const char* key, uint32_t family, const char* address, amxc_var_t* const ret_object);

/**
 *  get all addresses of the device with given key.
 *
 * @param target_addresses: If no errors, will contain a hashtable with keys the address and values
 *   hashtables describing properties of the address.
 */
amxd_status_t gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses);

/**
 *  set parameters of an address from a specific device
 */
amxd_status_t gmap_ip_device_set_address(const char* key, uint32_t family, const char* address, const char* scope, const char* status_value, const char* address_source, bool reserved);


/**
 *  give the family (AF_INET6, AF_INET) for a specific address
 */
uint32_t gmap_ip_family(const char* ipaddress);

/**
 *  give the family (AF_INET6, AF_INET) for a spesific type of ipXaddress (ipv4, ipv6)
 */
uint32_t gmap_ip_family_string2id(const char* family);

/**
 *  give the ip family string representation ("ipv4", "ipv6") for the given ip family id.
 */
const char* gmap_ip_family_id2string(uint32_t family);

/**
 *  whether the given address family is a valid IP family.
 *
 * An address family that is not an IP address family (e.g. IPX (Internetwork Packet Exchange))
 * is not considered a valid IP family.
 */
bool gmap_ip_family_is_valid(uint32_t family);

/**
 *  give the family template object name ("IPv4Address", "IPv6Address") for the given IP family
 *
 * @param family: @ref AF_INET or @ref AF_INET6.
 * @return "IPv4Address" or "IPv6Address", or NULL on invalid argument.
 */
const char* gmap_ip_family_id_to_objname(uint32_t family);

/**
 *  give the family tag name ("ipv4", "ipv6") for the given IP family
 *
 * @param family: @ref AF_INET or @ref AF_INET6.
 * @return "ipv4" or "ipv6", or NULL on invalid argument.
 */
const char* gmap_ip_family_id_to_tag(uint32_t family);

/**
 *  give a good name/string for a type of ip_address_scope
 */
const char* gmap_ip_address_scope2string(gmap_ip_address_scope_t scope);

/**
 *  give the scope for a specific ipaddress
 */
amxd_status_t gmap_ip_addressScope(uint32_t family, const char* address, gmap_ip_address_scope_t* const ret_scope);

#ifdef __cplusplus
}
#endif

#endif // __GMAP_IPADDRESS_H__
