/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAP_IPQUERY_H__)
#define __GMAP_IPQUERY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <stdbool.h>
#include <stdint.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>

/**
 * Callback called when Ip address
 * - is added or changed and status is reachable (ip_active = true)
 * - is removed or status is no longer reachable (ip_active = false)
 *
 * In case of an IP address change, this callback is called twice (first with `ip_active=false` and
 *   `ip` the old IP, next as `ip_active=true` and `ip` the new IP).
 *
 * In case of a Status change, this callback is called once ( `ip_active=false` if new status i no longer reachable,
 * `ip_active=true` when the new status is reachable).
 *
 * @param ip_active: true if the IP is added or changed and Status is 'reachable',
 *   false if it is removed or status changes to anything but 'reachable'.
 */
typedef void (* gmap_ipquery_cb_t) (const char* dev_key,
                                    const char* ip,
                                    bool ip_active,
                                    void* const userdata);

typedef struct gmap_ipquery gmap_ipquery_t;

/**
 * Call callback on new (or initially existing) IP and removed IP for matching devices.
 *
 * @param family IP address family (@ref AF_INET or @ref AF_INET6)
 * @param tag Only watch devices that have this tag. Currently only a single or no tag is supported.
 * @param callback Will be called when the set of matching IPs changes, and also initially
 *   for each IP that matches when this query is created. These initial callback calls happen
 *   immediately (before `gmap_ipquery_new` returns).
 *   There is no guarantee that the former (set of matching IP changes) and the latter (initial
 *   calls) do not overlap, so the callback can be called twice immediately after each other
 *   for the same IP.
 * @param userdata: data of user to be passed to the callback.
 */
bool gmap_ipquery_new(gmap_ipquery_t** ipquery, uint32_t family, UNUSED const char* tag,
                      gmap_ipquery_cb_t callback, void* userdata);

void gmap_ipquery_delete(gmap_ipquery_t** ipquery);

#ifdef __cplusplus
}
#endif
#endif
