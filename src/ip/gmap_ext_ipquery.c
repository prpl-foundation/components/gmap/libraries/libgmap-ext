/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap/extensions/ip/gmap_ext_ipquery.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxb/amxb_subscribe.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <string.h>

#include <gmap/gmap_main.h>
#include <gmap/gmap_query.h>
#include "gmap/extensions/ip/gmap_ext_ipaddress.h"

#define ME "libgmap"

struct gmap_ipquery {
    amxb_subscription_t* subscription;
    gmap_ipquery_cb_t callback;
    void* userdata;
};

static bool s_addr_key_of_ip_notif(const amxc_var_t* const notif_data, char** address, char** status) {
    amxc_var_t ret;
    int retval = -1;
    bool res = false;
    const char* notification = NULL;
    const char* notif_path = NULL;

    amxc_var_init(&ret);

    notification = GET_CHAR(notif_data, "notification");
    notif_path = GET_CHAR(notif_data, "path");

    when_null_trace(notification, exit, ERROR, "notification NULL");
    when_null_trace(notif_path, exit, ERROR, "path NULL");

    retval = amxb_get(gmap_get_bus_ctx(), notif_path, 0, &ret, gmap_get_timeout());
    when_failed_trace(retval, exit, ERROR, "Error on amxb_get() %s", notif_path);

    *address = amxc_var_take(cstring_t, GET_ARG(amxc_var_get_first(amxc_var_get_first(&ret)), "Address"));
    *status = amxc_var_take(cstring_t, GET_ARG(amxc_var_get_first(amxc_var_get_first(&ret)), "Status"));

    res = true;

exit:
    amxc_var_clean(&ret);
    return res;
}

static char* s_dev_key_of_path(const char* path_part, int remove_depth) {
    char* key = NULL;
    amxc_var_t ret;
    amxd_path_t key_path;
    int status = -1;

    amxd_path_init(&key_path, path_part);
    amxc_var_init(&ret);

    for(int i = 0; i < remove_depth; i++) {
        free(amxd_path_get_last(&key_path, true));
    }
    status = amxb_get(gmap_get_bus_ctx(), amxd_path_get(&key_path, AMXD_OBJECT_TERMINATE), 0, &ret, gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Error on amxb_get() %s Key", amxd_path_get(&key_path, AMXD_OBJECT_TERMINATE));

    amxc_var_dump(&ret, 1);

    key = amxc_var_take(cstring_t, GET_ARG(amxc_var_get_first(amxc_var_get_first(&ret)), "Key"));

exit:
    amxc_var_clean(&ret);
    amxd_path_clean(&key_path);

    return key;
}

static char* s_dev_key_of_ip_notif(const amxc_var_t* const notif_data) {
    const char* notification = GET_CHAR(notif_data, "notification");
    const char* notif_path = GET_CHAR(notif_data, "path");
    when_null_trace(notification, error, ERROR, "NULL");
    when_null_trace(notif_path, error, ERROR, "NULL");

    if(0 == strcmp(notification, "dm:object-changed")) {
        return s_dev_key_of_path(notif_path, 2);
    } else {
        return s_dev_key_of_path(notif_path, 1);
    }

error:
    return NULL;
}

/**
 * @implements amxp_slot_fn_t
 */
static void s_on_address_notif(const char* const sig_name UNUSED,
                               const amxc_var_t* const notif_data,
                               void* const priv) {
    char* dev_key = NULL;
    gmap_ipquery_t* ipquery = priv;
    const char* path = GET_CHAR(notif_data, "path");
    const char* notification = GET_CHAR(notif_data, "notification");
    when_null_trace(ipquery, exit, ERROR, "NULL");
    when_str_empty_trace(path, exit, ERROR, "Failed to obtain object path");
    when_str_empty_trace(notification, exit, ERROR, "No notification type");
    when_null(ipquery->callback, exit);

    dev_key = s_dev_key_of_ip_notif(notif_data);
    when_null_trace(dev_key, exit, ERROR, "Error getting device key");

    if(0 == strcmp(notification, "dm:instance-added")) {
        const char* address = GETP_CHAR(notif_data, "parameters.Address");
        const char* status = GETP_CHAR(notif_data, "parameters.Status");
        when_str_empty(address, exit);
        when_str_empty(status, exit);
        if(strcmp(status, GMAP_IP_STATUS_REACHABLE) == 0) {
            ipquery->callback(dev_key, address, true, ipquery->userdata);
        }
    } else if(0 == strcmp(notification, "dm:instance-removed")) {
        const char* address = GETP_CHAR(notif_data, "parameters.Address");
        const char* status = GETP_CHAR(notif_data, "parameters.Status");
        when_str_empty(address, exit);
        when_str_empty(status, exit);
        if(strcmp(status, GMAP_IP_STATUS_REACHABLE) == 0) {
            ipquery->callback(dev_key, address, false, ipquery->userdata);
        }
    } else if(0 == strcmp(notification, "dm:object-changed")) {
        char* address = NULL;
        char* status = NULL;
        const char* address_from = GETP_CHAR(notif_data, "parameters.Address.from");
        const char* status_from = GETP_CHAR(notif_data, "parameters.Status.from");
        if((address_from == NULL) && (status_from == NULL)) {
            // Nothing relevant changed
            goto exit;
        }

        when_false(s_addr_key_of_ip_notif(notif_data, &address, &status), exit);
        if(status_from == NULL) {
            status_from = status;
        }
        if(address_from == NULL) {
            address_from = address;
        }

        if(strcmp(status_from, GMAP_IP_STATUS_REACHABLE) == 0) {
            ipquery->callback(dev_key, address_from, false, ipquery->userdata);
        }

        if(strcmp(status, GMAP_IP_STATUS_REACHABLE) == 0) {
            ipquery->callback(dev_key, address, true, ipquery->userdata);
        }

        free(address);
        free(status);
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown notification %s", notification);
    }

exit:
    free(dev_key);
    return;
}

static void s_call_callback_on_existing_ips(const char* path_expr, gmap_ipquery_cb_t callback, void* userdata) {
    amxc_var_t ret;
    int retval = -1;
    amxc_var_init(&ret);
    when_null_trace(path_expr, exit, ERROR, "NULL");
    when_null(callback, exit);

    retval = amxb_get(gmap_get_bus_ctx(), path_expr, 1, &ret, gmap_get_timeout());
    when_failed(retval, exit);
    amxc_var_for_each(params, amxc_var_get_first(&ret)) {
        const char* path = amxc_var_key(params);
        const char* ip = NULL;
        const char* status = NULL;
        char* key = NULL;
        if(amxc_htable_is_empty(amxc_var_constcast(amxc_htable_t, params))) {
            // Ignore "Devices.Device.1.IPv4Address. = {}" entry.
            continue;
        }
        ip = GET_CHAR(params, "Address");
        if((ip == NULL) || (ip[0] == '\0')) {
            continue;
        }

        status = GET_CHAR(params, "Status");
        if((status == NULL) || (status[0] == '\0') || (strcmp(status, GMAP_IP_STATUS_REACHABLE) != 0)) {
            continue;
        }

        key = s_dev_key_of_path(path, 2);
        if(key == NULL) {
            SAH_TRACEZ_ERROR(ME, "Error getting key for %s", path);
            continue;
        }

        callback(key, ip, true, userdata);
        free(key);
    }

exit:
    amxc_var_clean(&ret);
}

bool gmap_ipquery_new(gmap_ipquery_t** ipquery, uint32_t family,
                      UNUSED const char* tag, gmap_ipquery_cb_t callback, void* userdata) {

    int status = -1;
    amxc_var_t ret;
    amxc_string_t path;
    bool retval = false;
    amxc_string_init(&path, 70);
    amxc_var_init(&ret);
    when_null_trace(ipquery, exit, ERROR, "NULL");
    when_false_trace(gmap_ip_family_is_valid(family), exit, ERROR, "Invalid IP family");

    *ipquery = calloc(1, sizeof(gmap_ipquery_t));
    when_null_trace(*ipquery, exit, ERROR, "Out of mem");
    (*ipquery)->callback = callback;
    (*ipquery)->userdata = userdata;

    status = amxc_string_setf(&path, "Devices.Device.*.%s.", gmap_ip_family_id_to_objname(family));
    when_failed_trace(status, exit, ERROR, "Error building path");

    s_call_callback_on_existing_ips(amxc_string_get(&path, 0), callback, userdata);

    status = amxb_subscription_new(&(*ipquery)->subscription, gmap_get_bus_ctx(),
                                   amxc_string_get(&path, 0),
                                   "notification in ['dm:instance-added', 'dm:instance-removed', 'dm:object-changed']",
                                   s_on_address_notif,
                                   *ipquery);

    when_failed_trace(status, exit, ERROR, "Error subscribing for addresses");

    retval = true;
exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    if(!retval) {
        gmap_ipquery_delete(ipquery);
    }
    return retval;
}

void gmap_ipquery_delete(gmap_ipquery_t** ipquery) {
    if((ipquery == NULL) || (*ipquery == NULL)) {
        return;
    }
    amxb_subscription_delete(&(*ipquery)->subscription);
    free(*ipquery);
    *ipquery = NULL;
}