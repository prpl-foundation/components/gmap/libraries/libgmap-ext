/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "gmap_ext_priv.h"
#include "gmap_ext_util.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_device.h>
#include "gmap/extensions/ip/gmap_ext_ipaddress.h"

#include "gmap/extensions/ip/gmap_ext_ip_devices.h"


char* gmap_devices_findByIp(const char* ip) {
    const char* ip_family = NULL;
    const char* dev_key_const = NULL;
    char* dev_key = NULL;
    amxc_var_t expression;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&expression);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&path, 0);
    amxc_var_set_type(&expression, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&ret, AMXC_VAR_ID_LIST);

    when_null_trace(ip, exit, ERROR, "NULL");
    ip_family = gmap_ip_family_id_to_objname(gmap_ip_family(ip));

    amxc_string_setf(&path, "has_instance(\"%s\", \".Address ^= '%s' && .Status == '%s'\")", ip_family, ip, GMAP_IP_STATUS_REACHABLE);

    gmap_devices_find(amxc_string_get(&path, 0), 0, &ret);

    dev_key_const = amxc_var_constcast(cstring_t, amxc_var_get_index(GETI_ARG(&ret, 0), 0, AMXC_VAR_FLAG_DEFAULT));
    dev_key = dev_key_const == NULL ? NULL : strdup(dev_key_const);

exit:
    amxc_var_clean(&expression);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&path);

    return dev_key;
}