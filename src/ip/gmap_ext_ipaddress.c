/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/
#include "gmap/extensions/ip/gmap_ext_ipaddress.h"
#include <gmap/gmap_main.h>
//#include "gmap_priv.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <arpa/inet.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_dm.h>
#include <amxb/amxb.h>

#include <gmap/gmap.h>
#include <gmap/gmap_devices.h>
#include <gmap/gmap_device.h>


/**********************************************************
* Macro definitions
**********************************************************/

#define GMAP_IP_PARAMETER_SCOPE "Scope"
#define GMAP_IP_PARAMETER_STATUS "Status"
#define GMAP_IP_PARAMETER_ADDRESS "Address"
#define GMAP_IP_PARAMETER_ADDRESS_SOURCE "AddressSource"
#define GMAP_IP_PARAMETER_RESERVED "Reserved"

#define ME "libgmap"

/**********************************************************
* Type definitions
**********************************************************/

static const char* ip_address_scope_strs[] = {
    GMAP_IP_SCOPE_UNKNOWN,
    GMAP_IP_SCOPE_LINK,
    GMAP_IP_SCOPE_SITE,
    GMAP_IP_SCOPE_GLOBAL,
};

/**********************************************************
* Function Prototypes
**********************************************************/

/**
 * create a string/path for an object, by the parameters
 */
static amxd_status_t gmap_ip_device_createPath(uint32_t family, const char* address, amxc_string_t* const ret_path);

/**
 * find if an ipv4 address starts with the prefix of a link ipv4 address
 */
static amxd_status_t gmap_ip_address_isAutoIP(uint32_t family, const char* address, bool* const is_auto_ip);

/**
 * give the scope (link, global) for an ipv4 address
 */
static amxd_status_t gmap_ip_v4_scope(const char* address, gmap_ip_address_scope_t* const ret_scope);

/**
 * give the scope (link, site, global) for an ipv6 address
 */
static void gmap_ip_v6_scope(struct in6_addr* ipv6address, gmap_ip_address_scope_t* const ret_scope);

/**********************************************************
* Functions
**********************************************************/

static amxd_status_t gmap_ip_device_createPath(uint32_t family, const char* address, amxc_string_t* const ret_path) {
    const char* family_object_name = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    family_object_name = gmap_ip_family_id_to_objname(family);
    if(family_object_name == NULL) {
        SAH_TRACEZ_ERROR(ME, "Invalid family parameter is given");
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    status = amxc_string_appendf(ret_path, "%s", family_object_name);
    when_failed(status, exit);

    if((address != NULL) && (*address != '\0')) {
        status = amxc_string_appendf(ret_path, ".[Address == '%s']", address);
        when_failed(status, exit);
    }

exit:
    return status;
}

static amxd_status_t gmap_ip_address_isAutoIP(uint32_t family, const char* address, bool* const is_auto_ip) {
    amxd_status_t status = amxd_status_unknown_error;
    struct in_addr ipv4_addr;
    int s = 0;
    int net = 0;
    unsigned char* buf = NULL;
    *is_auto_ip = false;

    if(!address || !(*address)) {
        status = amxd_status_invalid_function_argument;
        SAH_TRACEZ_ERROR(ME, "Empty address");
        goto exit;
    }

    if(family == AF_INET) {// only in ipv4
        buf = (void*) &ipv4_addr;
    } else {
        status = amxd_status_invalid_function_argument;
        SAH_TRACEZ_ERROR(ME, "Invalid ip family");
        goto exit;
    }

    s = inet_pton(family, address, buf);//addres to binairy form (ret 1 on success)
    if(s <= 0) {
        status = amxd_status_invalid_function_argument;
        if(s == 0) {
            SAH_TRACEZ_ERROR(ME, "Address %s not in presentation format", address);
        } else {
            SAH_TRACEZ_ERROR(ME, "Address %s error inet_pton", address);
        }
        goto exit;
    }

    net = inet_netof(ipv4_addr);
    SAH_TRACEZ_INFO(ME, "IPv4 network part = %X", net);
    if((net & 0xA9FE) == 0xA9FE) {
        *is_auto_ip = true;
    }

    status = amxd_status_ok;
exit:
    return status;
}

static amxd_status_t gmap_ip_v4_scope(const char* address, gmap_ip_address_scope_t* const ret_scope) {
    bool is_auto_ip = false;
    amxd_status_t status = gmap_ip_address_isAutoIP(AF_INET, address, &is_auto_ip);
    when_failed(status, exit);

    if(is_auto_ip) {
        *ret_scope = scope_link; //169.254.x.x
    } else {
        *ret_scope = scope_global;
    }

exit:
    return status;
}

static void gmap_ip_v6_scope(struct in6_addr* ipv6address, gmap_ip_address_scope_t* const ret_scope) {
    if(IN6_IS_ADDR_LINKLOCAL(ipv6address)) {
        *ret_scope = scope_link;
    } else if(IN6_IS_ADDR_SITELOCAL(ipv6address)) {
        *ret_scope = scope_site;
    } else {
        *ret_scope = scope_global;
    }
}


amxd_status_t gmap_ip_device_delete_address(const char* key, uint32_t family, const char* address) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t values;
    amxc_var_t args;
    amxc_var_t retval;
    amxc_string_t obj_path;
    const char* family_object_name = gmap_ip_family_id_to_objname(family);

    amxc_var_init(&values);
    amxc_var_init(&args);
    amxc_var_init(&retval);
    amxc_string_init(&obj_path, 0);

    when_str_empty_status(key, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(address, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(family_object_name, exit, status = amxd_status_parameter_not_found);

    amxc_string_setf(&obj_path, "Devices.Device.%s.%s.", key, family_object_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "address", address);

    status = amxb_call(gmap_get_bus_ctx(), amxc_string_get(&obj_path, 0), "deleteAddressInstance", &args, &retval, gmap_get_timeout());

    when_failed_trace(status, exit, ERROR, "Error deleting address %s from %s", address, key);
    SAH_TRACEZ_INFO(ME, "Successfully deleted an object %s", amxc_string_get(&obj_path, 0));

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&args);
    amxc_var_clean(&retval);
    amxc_string_clean(&obj_path);
    return status;
}

amxd_status_t gmap_ip_device_add_address(const char* key, uint32_t family, const char* address, const char* scope, const char* status_value, const char* address_source, bool reserved) {
    const char* family_object_name = gmap_ip_family_id_to_objname(family);
    amxd_status_t status = amxd_status_ok;
    amxc_var_t param;
    amxc_var_t retval;
    amxc_string_t path;

    amxc_var_init(&param);
    amxc_var_init(&retval);
    amxc_string_init(&path, 0);
    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    when_str_empty_status(key, exit, status = amxd_status_parameter_not_found);
    when_null_status(family_object_name, exit, status = amxd_status_parameter_not_found);

    amxc_var_add_key(cstring_t, &param, "scope", scope);
    amxc_var_add_key(cstring_t, &param, "status", status_value);
    amxc_var_add_key(cstring_t, &param, "address", address);
    amxc_var_add_key(cstring_t, &param, "source", address_source);

    if(family == AF_INET) {
        amxc_var_add_key(bool, &param, "reserved", reserved);
    }

    amxc_string_setf(&path, "Devices.Device.%s.%s.", key, family_object_name);
    status = amxb_call(gmap_get_bus_ctx(), amxc_string_get(&path, 0), "addAddressInstance", &param, &retval, gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Failed to create an %s instance for %s: -> check if mib_ip is installed or the correct ipv4/ipv6 tag is added", family_object_name, key);

    SAH_TRACEZ_INFO(ME, "Successfully added an %s instance to %s", family_object_name, key);

exit:
    amxc_var_clean(&param);
    amxc_var_clean(&retval);
    amxc_string_clean(&path);
    return status;
}

/**
 *
 * @param address Can be NULL.
 * @param target: the output parameter
 */
static amxd_status_t s_get_address_or_addresses(const char* key, uint32_t family, const char* address, amxc_var_t* target) {
    amxd_status_t status = amxd_status_parameter_not_found;
    amxc_var_t* retval_obj = NULL;
    amxc_var_t ret;
    amxc_string_t obj_path;
    amxc_var_init(&ret);
    amxc_string_init(&obj_path, 0);
    when_str_empty_trace(key, exit, ERROR, "NULL/Empty");
    when_null_trace(target, exit, ERROR, "NULL");

    status = gmap_ip_device_createPath(family, address, &obj_path);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to create the object path, with key: %s", key);
        goto exit;
    }

    status = gmap_get_subobject_instance(key, amxc_string_get(&obj_path, 0), 0, &ret);

    // Note: This can legitimately happen in case it does not exist while the caller does not assume
    //       it exists. So trace level INFO instead of ERROR.
    when_failed_trace(status, exit, INFO, "Did not find an object with path %s", amxc_string_get(&obj_path, 0));
    retval_obj = GETI_ARG(&ret, 0);
    if(amxc_var_is_null(retval_obj)) {
        SAH_TRACEZ_INFO(ME, "Did not find an object with path %s", amxc_string_get(&obj_path, 0));
        status = amxd_status_object_not_found;
        goto exit;
    }

    amxc_var_move(target, retval_obj);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&obj_path);
    return status;
}

amxd_status_t gmap_ip_device_get_address(const char* key, uint32_t family, const char* address, amxc_var_t* const ret_object) {
    amxd_status_t status = amxd_status_ok;
    amxc_var_t* retval_obj = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);
    when_str_empty_status(key, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(address, exit, status = amxd_status_parameter_not_found);
    when_null_status(ret_object, exit, status = amxd_status_parameter_not_found);

    status = s_get_address_or_addresses(key, family, address, &ret);
    when_failed_trace(status, exit, INFO, "Address not found %s %s", key, address);

    retval_obj = GETI_ARG(&ret, 0);
    if(!amxc_var_is_null(retval_obj)) {
        SAH_TRACEZ_INFO(ME, "Found address %s for %s", address, key);
        amxc_var_copy(ret_object, retval_obj);
    } else {
        status = amxd_status_object_not_found;
        SAH_TRACEZ_INFO(ME, "Address not found %s %s", key, address);
    }

exit:
    amxc_var_clean(&ret);
    return status;
}

/**
 * Changes
 * ```{"Some.Path.SomeWhere" => {"Address": "192.168.1.2", "Foo": "bar", ...}, ...}```
 * into
 * ```{"192.168.1.2" => {"Address": "192.168.1.2", "Foo": "bar", ...}, ...}```.
 */
static void s_rename_key_to_address(amxc_var_t* src_addresses, amxc_var_t* tgt_addresses) {
    amxc_var_for_each(src_addr_fields, tgt_addresses) {
        const char* ip = GET_CHAR(src_addr_fields, "Address");
        const amxc_htable_t* src_addr_fields_htable = amxc_var_constcast(amxc_htable_t, src_addr_fields);
        when_null_trace(src_addr_fields_htable, exit, ERROR, "NULL");
        if(ip == NULL) {
            // Ignore the `"Devices.Device.<mydev>.IPv4Address." = {}` entry.
            continue;
        }
        if(amxc_var_get_key(tgt_addresses, ip, AMXC_VAR_FLAG_DEFAULT) != NULL) {
            SAH_TRACEZ_ERROR(ME, "Duplicate %s", ip);
            continue;
        }
        amxc_var_add_new_key_amxc_htable_t(src_addresses, ip, src_addr_fields_htable);
    }

exit:
    return;
}

amxd_status_t gmap_ip_device_get_addresses(const char* key, amxc_var_t* target_addresses) {
    amxc_var_t ret;
    amxd_status_t status = amxd_status_parameter_not_found;
    amxc_var_init(&ret);

    when_str_empty_trace(key, exit, ERROR, "NULL/Empty");
    when_null_trace(target_addresses, exit, ERROR, "NULL");
    amxc_var_set_type(target_addresses, AMXC_VAR_ID_HTABLE);

    status = s_get_address_or_addresses(key, AF_INET, NULL, &ret);
    when_failed_trace(status, exit, INFO, "Error retrieving ipv4 addresses for %s", key);
    s_rename_key_to_address(target_addresses, &ret);

    amxc_var_clean(&ret);
    status = s_get_address_or_addresses(key, AF_INET6, NULL, &ret);
    when_failed_trace(status, exit, INFO, "Error retrieving ipv6 addresses for %s", key);
    s_rename_key_to_address(target_addresses, &ret);

    status = amxd_status_ok;
exit:
    amxc_var_clean(&ret);
    return status;
}

amxd_status_t gmap_ip_device_set_address(const char* key, uint32_t family, const char* address, const char* scope, const char* status_value, const char* address_source, bool reserved) {
    amxd_status_t status = amxd_status_ok;
    const char* family_object_name = gmap_ip_family_id_to_objname(family);
    amxc_var_t retval;
    amxc_var_t param;
    amxc_string_t path;

    amxc_var_init(&param);
    amxc_var_init(&retval);
    amxc_string_init(&path, 0);

    when_str_empty_status(key, exit, status = amxd_status_parameter_not_found);
    when_str_empty_status(address, exit, status = amxd_status_parameter_not_found);

    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &param, "address", address);
    amxc_var_add_key(cstring_t, &param, "scope", scope);
    amxc_var_add_key(cstring_t, &param, "status", status_value);
    amxc_var_add_key(cstring_t, &param, "source", address_source);
    if(family == AF_INET) {
        amxc_var_add_key(bool, &param, "reserved", reserved);
    }

    amxc_string_setf(&path, "Devices.Device.%s.%s", key, family_object_name);
    status = amxb_call(gmap_get_bus_ctx(), amxc_string_get(&path, 0), "setAddressInstance", &param, &retval, gmap_get_timeout());
    when_failed_trace(status, exit, ERROR, "Failed to set new value for instance with address %s", address);

    SAH_TRACEZ_INFO(ME, "Successfully set an object %s", amxc_string_get(&path, 0));
exit:
    amxc_var_clean(&retval);
    amxc_var_clean(&param);
    amxc_string_clean(&path);
    return status;
}

uint32_t gmap_ip_family(const char* ipaddress) {
    char* pos = NULL;
    when_null_trace(ipaddress, error, ERROR, "NULL");
    pos = strchr(ipaddress, ':');

    if(pos) {
        return AF_INET6;
    } else {
        return AF_INET;
    }

error:
    return AF_INET;
}

uint32_t gmap_ip_family_string2id(const char* family) {
    uint32_t id = 0;

    if((family != NULL) && (family[0] != '\0')) {
        if(strcasecmp(family, "ipv4") == 0) {
            id = AF_INET;
        } else if(strcasecmp(family, "ipv6") == 0) {
            id = AF_INET6;
        }
    }

    return id;
}

const char* gmap_ip_family_id2string(uint32_t family) {
    if(family == AF_INET) {
        return "ipv4";
    } else if(family == AF_INET6) {
        return "ipv6";
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid IP family");
        return NULL;
    }
}

bool gmap_ip_family_is_valid(uint32_t family) {
    return family == AF_INET || family == AF_INET6;
}

const char* gmap_ip_family_id_to_objname(uint32_t family) {
    if(family == AF_INET) {
        return "IPv4Address";
    } else if(family == AF_INET6) {
        return "IPv6Address";
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid IP family");
        return NULL;
    }
}

const char* gmap_ip_family_id_to_tag(uint32_t family) {
    if(family == AF_INET) {
        return "ipv4";
    } else if(family == AF_INET6) {
        return "ipv6";
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid IP family");
        return NULL;
    }
}

const char* gmap_ip_address_scope2string(gmap_ip_address_scope_t scope) {
    if(scope <= scope_global) {
        return ip_address_scope_strs[scope];
    }
    return ip_address_scope_strs[scope_unknown];
}

amxd_status_t gmap_ip_addressScope(uint32_t family, const char* address, gmap_ip_address_scope_t* const ret_scope) {
    amxd_status_t status = amxd_status_unknown_error;
    struct in6_addr ipv6_addr;
    struct in_addr ipv4_addr;
    int s = 0;
    unsigned char* buf = NULL;
    *ret_scope = scope_unknown;

    switch(family) {
    case AF_INET:
        buf = (void*) &ipv4_addr;
        break;
    case AF_INET6:
        buf = (void*) &ipv6_addr;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Invalid ip family");
        status = amxd_status_invalid_function_argument;
        goto exit;
        break;
    }

    //convert to binary form (ret 1 on success)
    s = inet_pton(family, address, buf);
    if(s <= 0) {
        status = amxd_status_invalid_function_argument;
        if(s == 0) {
            SAH_TRACEZ_ERROR(ME, "Address %s not in presentation format", address);
        } else {
            SAH_TRACEZ_ERROR(ME, "Address %s error inet_pton", address);
        }
        goto exit;
    }

    switch(family) {
    case AF_INET:
        status = gmap_ip_v4_scope(address, ret_scope);
        when_failed(status, exit);
        break;
    case AF_INET6:
        gmap_ip_v6_scope(&ipv6_addr, ret_scope);
        break;
    }

    status = amxd_status_ok;
exit:
    return status;
}
